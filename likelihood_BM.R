#################################
## likelihood calculation in R ##
#################################

require(ape);

# We assume in this function that the tree is fully dichotomous and that 2 descendants are provided
#	for each internal node.
# The parameter sigma is the standard deviation of the rate of evolution of the trait.

# The branch lengths for internal nodes should be extended a bit because the variance of the internal
#	nodes is a bit larger than what is observed on the tree
#	see eqn 23.46 on page 405 in Joe's book.
# However, condvec[,3] stores the standard deviation of S, which is sigma^2 * (v1v2) / (v1 + v2) if 
#	you consider two descendants with branch lengths v1 and v2. We want here to recover only
#	(v1v2) / (v1 + v2), so we need to divide it by sigma^2 and take the square of it.
# The value in condvec[,3] is 0 for terminal taxa, where the branch lengths are "correct".

# The ancestral value is simply the weighted mean of the values, using the modified branch lengths
#	as the variance (modified because for internal nodes the variance is a bit larger).
#	see eqn 23.43 on page 405 in Joe's book

# Extra variance due to the composite construction of the ancestor based on the two descendants
#	given here is sigma^2 (v1 v2) / (v1 + v2)
#	see eqn 23.46 on page 405 in Joe's book
# Here, we store the standard deviation and we thus take the square-root of the variance.

# The likelihood is then simply the density of the normal distribution for the contrast of the two values

fN<-function(xa, xb, vpa, vpb, sigma2) {
  #the normal density. Sigma2 is the variance. Same as log(dnorm(xa-xb, 0, sqrt(sigma * sigma * (vpa + vpb))))
  return((-(xa - xb)^2 / (2 * sigma2 * (vpa + vpb)) - log(sqrt(2 * pi * sigma2 * (vpa + vpb)))));
}

fN2<-function(xa, xb, vpa, vpb, sigma2, anc) {
  #same as fN but with a known ancestor instead of xa-xb, see Joe's book eqn 23.10 (some mistakes in the denominator though in his equation)
  return(-(((xa - anc)^2 / vpa) + ((xb - anc)^2 / vpb)) / (2 * sigma2) - log(sqrt(2 * pi * sigma2 * (vpa + vpb))));
}

condLike<-function(sigma2=1.0, condvec, kids) {
  #following the paper by Felsenstein 1985 + Novembre and Slatkin 2008
  #given two descendants {a, b} and an ancestor c:
  #v'_a = v_a + S^2_a
  #v'_b = v_b + S^2_b
  #L_c = N(x_a - x_b, (v'_a + v'_b)*sigma^2)
  #S^2_c = v'_a * v'_b / (v'_a) + v'_b)
  #x_c = (v'_b * x_a) / (v'_a + v'_b) + (v'_a * x_b) / (v'_a + /v'_b)
  
  a <- kids[1];
  b <- kids[2];
  vpa <- condvec[a, 4] + condvec[a, 3];
  vpb <- condvec[b, 4] + condvec[b, 3];
  anc <- ((condvec[a, 2] * vpb) / (vpa + vpb)) + ((condvec[b, 2] * vpa) / (vpa + vpb))
  
  lnL <- fN2(condvec[a, 2], condvec[b, 2], vpa, vpb, sigma2, anc) + condvec[a, 1] + condvec[b, 1];# the log likelihood for the node
  
  S <- (vpa * vpb) / (vpa + vpb);
  
  return(c(lnL, anc, S));
}

#set the condL for the tips based on the data and the internal nodes
condvecInit<-function(tree, data, ntips, tnodes) {
  condvec<-matrix(nrow=tnodes, ncol=4); #four columns: the first for the logLn, the second for the state, the third for extra BM variance and the fourth is the brlen subtending the node

  for(i in 1:tnodes) {
    row<-which(tree$edge[,2] == i);
    if(i <= ntips) {
      condvec[row, 1] <- 0.0 #Log likelihood
      condvec[row, 2] <- data[i]; #data for this taxa
      condvec[row, 3] <- 0.0; #extra variance brought when computing the BM on trees
      condvec[row, 4] <- tree$edge.length[row]; #branch length below the node
    }
    else {
      condvec[row, 1] <- 0.0 #Log likelihood
      condvec[row, 2] <- 0.0; #data for this taxa
      condvec[row, 3] <- 0.0; #extra variance brought when computing the BM on trees
      if(i > (ntips+1)) {
	condvec[row, 4] <- tree$edge.length[row]; #branch length below the node
      }
      else {
	condvec[row, 4] <- 0.0; #branch length for the root, which is 0.0, usually
      }
    }
  }

  return(condvec);
}

lnLike<-function(tree, data, sigma2, getLnl=TRUE) {
  ntips<-length(tree$tip.label);
  nnodes<-tree$Nnode;
  tnodes<-ntips+nnodes;

  #conditional likelihood vectors
  condvec<-condvecInit(tree=tree, data=data, ntips=ntips, tnodes=tnodes);

  #do some kind of tree traversal, starting from the node
  #with the highest node value (because of the sorting in ape)
  for(i in tnodes:(ntips+2)) {
    row<-which(tree$edge[,2] == i);
    kids<-which(tree$edge[,1] == i);
    condvec[row, 1:3]<-condLike(sigma2=sigma2, condvec=condvec, kids);
  }

  #lnL<-tmp;
  #finish with the root
  kids<-which(tree$edge[,1]==(ntips+1));
  condvec[tnodes, 1:3]<-condLike(sigma2=sigma2, condvec=condvec, kids)
  
  if(getLnl) {
    return(condvec[tnodes, 1] + fN(condvec[tnodes, 2], condvec[tnodes, 2], condvec[tnodes, 3], 0, sigma2));
  }
  else {
    return(condvec[, 2]);
  }
}

fitBM <- function(tree, x, anc, sigma2, pruning=TRUE, optim=TRUE, getLnl=TRUE) {
  n <- length(x);
  
  if(optim) {
    if(pruning) {
      lik <- function(par, tree, x) lnLike(tree, x, par, getLnl=TRUE); #if you want to optimize, the function MUST return the likelihood, whatever the wish of the user...
      fit <- optim(c(1), lik, tree = tree, x = x, method = "L-BFGS-B", lower = c(1e-12), upper = c(Inf), control = list(fnscale = -1));
      return(list(sig2 = fit$par, logL = fit$value, convergence = fit$convergence));
    }
    else {
      require(mnormt);
      lik <- function(par, tree, x, n) sum(dmnorm(x, mean = rep(par[1], n), varcov = par[2] * vcv(tree), log = TRUE));
      fit <- optim(c(0, 1), lik, tree = tree, x = x, n = n, method = "L-BFGS-B", lower = c(-Inf, 1e-12), upper = c(Inf, Inf), control = list(fnscale = -1));
      return(list(x0 = fit$par[1], sig2 = fit$par[2], logL = fit$value, convergence = fit$convergence));
    }
  }
  else {
    if(pruning) {
      return(lnLike(tree, x, sigma2, getLnl=getLnl));
    }
    else {
      require(mnormt);
      return(sum(dmnorm(x, mean = rep(anc, n), varcov = sigma2 * vcv(tree), log = TRUE)));
    }
  }
}

###########################
## Testing the functions ##
###########################

require(phytools);

#### testing if the pruning algorithm or the typical multinormal density give the same likelhood: yes they do!
tree<-pbtree(n=50, scale=1);
data<-data.frame(char=fastBM(tree, sig2=2, a=0), row.names=tree$tip.label);
  
anc2<-fitBM(tree, data[,1], 0, 2.5, pruning=FALSE, optim=TRUE)$x0

s <- seq(0.1, 4.5, by=0.1)
res1 <- numeric(length(s))
res2 <- numeric(length(s))
for(i in 1:length(s)) {
  res1[i] <- fitBM(tree, data[,1], sigma2=s[i], pruning=TRUE, optim=FALSE, getLnl=TRUE)
  res2[i] <- fitBM(tree, data[,1], -0.880113, s[i], pruning=FALSE, optim=FALSE)
}

plot(s, res2, t="l", col="red")
lines(s, res1, t="l")

### testing the speed up. The pruning is much faster for large trees:
tree<-pbtree(n=500, scale=1);
data<-data.frame(char=fastBM(tree, sig2=2, a=0), row.names=tree$tip.label);

system.time(fitBM(tree, data[,1], 0.646066, 0.816942, pruning=FALSE, optim=FALSE))
system.time(fitBM(tree, data[,1], 10, sigma2=0.816942, pruning=TRUE, optim=FALSE))

### testing the accuracy of the parameter estimates
res<-numeric(length=100)
anc<-numeric(length=100)
trueS<-0.5
trueA<-1.0
for(i in 1:100) {
  tree<-pbtree(n=20, scale=1);
  data<-data.frame(char=fastBM(tree, sig2=trueS, a=trueA), row.names=tree$tip.label);
  
  res[i]<-fitBM(tree, data[,1], sigma2=2.0, pruning=TRUE, optim=TRUE, getLnl=TRUE)$sig2
  anc[i]<-fitBM(tree, data[,1], sigma2=res[i], pruning=TRUE, optim=FALSE, getLnl=FALSE)[9]
}

plot(res, ylim=c(0,2*trueS))
abline(h=trueS, col="red")

plot(anc, ylim=c(0, 3*trueA))
abline(h=trueA, col="red")

########################
### Running the code ###
########################

tree<-pbtree(n=20, scale=1);
data<-data.frame(char=fastBM(tree, sig2=2, a=0), row.names=tree$tip.label);

#step 1: estimate the sigma
s2<-fitBM(tree, data[,1], pruning=TRUE, optim=TRUE, getLnl=TRUE)$sig2

#step 2: getting the values at each node:
fitBM(tree, data[,1], sigma2=s2, pruning=TRUE, optim=FALSE, getLnl=FALSE)